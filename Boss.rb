require 'gtk3'
require 'json'
require_relative 'ToggleableEntity'
require_relative 'BossesUtility'

class Boss < ToggleableEntity
	attr_reader :dir, :nom, :btn
	
	@@curlSemaphore=Mutex.new
	
	def initialize(name, path, wing)
		@dir=Dir.new(path)
		@downloaded=false
		@nom=name
		super(@nom, 'boss')
		self.btn.signal_connect('toggled') do
			self.update(wing)
			false
		end
	end
	
	def update(wing) # un simple !@btn.active? crash ; vérifier pourquoi 
		@btn.active=(@btn.active?? true : false)
		# wing.btn.active=true if @btn.active?	
	end
	

	def initLogsPosition(list)
		@iter=list.append
		@iter[0]="" # possible source de crash
	end

	def updateFullWing(active)
		@btn.active=active
	end
	
	def upload(list, amount_raw)
		return unless @btn.active?
		return if @downloaded
		@downloaded=true

		amount_actual=amount_raw-1
		Thread.new { 
			@@curlSemaphore.synchronize {
				files=Array.new(amount_actual)
				arr_files_raw=Dir.children(@dir).sort_by { |f| File.mtime(@dir.path+"/"+f) }.reverse					
				
				(amount_actual+1).times { |i|
					files[i]=@dir.path+"/"+arr_files_raw[i]
				}
				files.each { |file|
					response= `curl -F json=1 -F file="@#{file}" https://dps.report/uploadContent`
					parsed=JSON.parse(response)
					@iter[0]+=parsed['permalink'].gsub('/\\', '/')+" #{@nom}\n"
				}
			}
		}

		
	end
end