require 'gtk3'
require_relative 'ToggleableEntity'

class Wing < Gtk::Box
	attr_accessor :bosses
	include Enumerable

	def initialize(number)
		super(Gtk::Orientation::VERTICAL)
		self.name='wing'
		@bosses=[]
		@name=calcName(number)
		@wingToggle=ToggleableEntity.new(@name, 'WingName')
		self.btn.signal_connect('toggled') do
			self.update
			false
		end

		self.add(@wingToggle)
	end
	
	def btn
		 @wingToggle.btn
	end
	
	
	def each(&block)
		@bosses.each(&block)
	end
	
	def update
		@bosses.each do |boss|
			boss.updateFullWing(self.btn.active?)
		end

	end
	def upload(list, amount)
		if self.btn.active?
			@iter[0]=@name+"\n"
		end
		@bosses.each { |b| b.upload(list, amount) }
		
	end
	
	def addBoss(boss, number)
		@bosses[number]=boss
	end
	
	def showWing
		@bosses.compact!
		@bosses.each do |b|
			self.add(b)
		end
	end
	
	def initLogsPosition(list)
		@iter=list.append

		@bosses.each { |b| b.initLogsPosition(list) }
	end
	
	private
	
	def calcName(number)
		return case number
		when 0 then "Autres"
		when 1..7 then "W"+number.to_s
		else number.to_s+" CM"
		end
	end
end