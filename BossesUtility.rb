# Comporte une série de méthodes permettant de lier le boss tel que connu couramment au dossier généré par Arc
class BossesUtility
	AMOUNT_WINGS=8
	@@lesserLogs=-1


	# Dans l'array de wings _wings_, crée un Boss _boss_ à la bonne position dans la bonne wing 
	def BossesUtility.generateBoss(bossDir, wings, list)
		actualName=BossesUtility.parseName(bossDir.split('/')[-1])
		association=BossesUtility.associateBoss(actualName)

		# association[0] : la wing du boss, indexée à 1
		# association[1] : la position dans la wing du boss, indexée à 0

		wings[association[0]]=Wing.new(association[0]) if wings[association[0]]==nil
		wings[association[0]].addBoss(Boss.new(actualName, bossDir, wings[association[0]]), association[1])
	end
	
	def BossesUtility.associateBoss(boss)
		return case boss
		when 'VG' then [1,0]
		# when 'Vale Guardian' then [0,0]
		when 'Gorseval' then [1,1]
		# when 'Gorseval the Multifarious' then [0,1]
		when 'Sabetha' then [1,2]
		# when 'Sabetha the Saboteur' then [0,2]
		when 'Sloth' then [2,0]
		# when 'Slothasor' then [1,0]
		when 'Trio' then [2,1]
		when 'Matthias' then [2,2]
		when 'KC' then [3,0]
		# when 'Keep Construct' then [2,0]
		when 'Xera' then [3,1]
		when 'Cairn' then [4,0]
		# when 'Cairn the Indomitable' then [3,0]
		when 'MO' then [4,1]
		# when 'Mursaat Overseer' then [3,1]
		when 'Samarog' then [4,2]
		when 'Deimos' then [4,3]
		when 'SH' then [5,0]
		# when 'Soulless Horror' then [4,0]
		when 'Dhuum' then [5,1]
		when 'CA' then [6,0]
		when 'Largos' then [6,1]
		when 'Qadim' then [6,2]
		when 'Adina' then [7,0]
		when 'Sabir' then [7,1]
		when 'PQadim' then [7,2]
		when 'MAMA' then [99,0]
		when 'Siax' then [99,1]
		when 'Ensolyss' then [99,2]
		when 'Skorvald' then [100,0]
		when 'Viirastra' then [100,1]
		when 'Arkk' then [100,2]
		else [0,@@lesserLogs+=1]
		end
	end
	
	def BossesUtility.parseName(boss)
		return case boss
		when 'Gardien de la Vallée' then 'VG'
		# when 'Vale Guardian' then [0,0]
		when 'Gorseval le Disparate' then 'Gorseval'
		# when 'Gorseval the Multifarious' then [0,1]
		when 'Sabetha la saboteuse' then 'Sabetha'
		# when 'Sabetha the Saboteur' then [0,2]
		when 'Paressor' then 'Sloth'
		# when 'Slothasor' then [1,0]
		when 'Berg' then 'Trio'
		when 'Matthias Gabrel' then 'Matthias'
		when 'Titan du fort' then 'KC'
		# when 'Keep Construct' then [2,0]
		when 'Xera' then 'Xera'
		when 'Cairn lIndomptable' then 'Cairn'
		# when 'Cairn the Indomitable' then [3,0]
		when 'Surveillant mursaat' then 'MO'
		# when 'Mursaat Overseer' then [3,1]
		when 'Samarog' then 'Samarog'
		when 'Deimos' then 'Deimos'
		when 'Horreur sans âme' then 'SH'
		# when 'Soulless Horror' then [4,0]
		when 'Dhuum' then 'Dhuum'
		when 'Amalgame conjuré' then 'CA'
		when 'Nikare' then 'Largos'
		when 'Qadim' then 'Qadim'
		when 'Cardinale Adina' then 'Adina'
		when 'Cardinal Sabir' then 'Sabir'
		when 'Qadim lInégalé' then 'PQadim'
		when 'AMAM' then 'MAMA'
		when 'Oratuss des Cauchemars' then 'Siax'
		when 'Ensolyss du Tourment éternel' then 'Ensolyss'
		when 'Skorvald le brisé' then 'Skorvald'
		when 'Artsariiv' then 'Viirastra'
		when 'Arkk' then 'Arkk'
		else boss
		end
	end
end