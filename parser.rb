hAbrevToName={  "vg"    => "VG", 
				"gors"  => "Gorseval", 
				"sab"   => "Sabetha",
				"sloth" => "Sloth",
				"matt"  => "Matthias",
				"kc"    => "KC",
				"xera"  => "Xera",
				"cairn" => "Cairn", 
				"mo"    => "MO",
				"sam"   => "Samarog",
				"dei"   => "Deimos",
				"sh"    => "SH",
				"dhuum" => "Dhuum",
				"ca"    => "CA",
				"twins" => "Largos", 
				"qadim" => "Qadim", 
				"adina" => "Adina", 
				"sabir" => "Sabir", 
				"qpeer" => "PQadim",

				"mama"  => "MAMA", 
				"siax"  => "Siax", 
				"enso"  => "Ensolyss", 
				"skor"  => "Skorvald", 
				"arriv" => "Viirastra", 
				"arkk"  => "Arkk", 
				"golem" => ""}

aIndexAbrevs=hAbrevToName.keys
	
aIndexToWing=[aIndexAbrevs.index("sab")+1,
			  aIndexAbrevs.index("matt")+1,
			  aIndexAbrevs.index("xera")+1,
			  aIndexAbrevs.index("dei")+1,
			  aIndexAbrevs.index("dhuum")+1,
			  aIndexAbrevs.index("qadim")+1,
			  aIndexAbrevs.index("qpeer")+1,
			  aIndexAbrevs.index("enso")+1,
			  aIndexAbrevs.index("arkk")+1,
			  aIndexAbrevs.index("golem")+1]

iNbWings=aIndexToWing.length
iNbRaidWings=7
			  
aWings=Array.new(iNbWings) { Array.new }

ARGV.each do |url|
	abrev=url.split('_')[-1] # identificateur du boss en fin d'URL
	index=aIndexAbrevs.index(abrev)
	name=hAbrevToName[abrev]
	
	unless name==nil
		sEndLog=url+" "+name+"\n"
		wing=0
		aIndexToWing.each { |i|	wing+=1 if i<=index }
		aWings[wing][index]=sEndLog
	end
end


iNbWings.times do |indice|
	title="\n"
	if indice<iNbRaidWings
		title+="W#{indice+1}"
	elsif indice!=iNbWings-1
		title+="#{indice+(100-iNbRaidWings-1)} CM"
	else
		title+="Golem"
	end
	puts title unless aWings[indice].empty?
	puts aWings[indice].compact
end