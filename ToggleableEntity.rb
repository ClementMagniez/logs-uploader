require 'gtk3'

# Retirer complètement ou remplacer par TitreWing, contient checkbutton et alignement de Boss

class ToggleableEntity < Gtk::Box

	attr_accessor :btn

	def initialize(nom, css)
		super(Gtk::Orientation::HORIZONTAL)
		self.name=css
		@btn=Gtk::CheckButton.new(nom)
		self.add(@btn)
	end
end
