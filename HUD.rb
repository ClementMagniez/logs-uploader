require 'gtk3'
require 'clipboard'
require 'pathname'
require_relative 'Boss'
require_relative 'Wing'
require_relative 'BossesUtility'

# Générer les bosses en leur associant un TreeIter


class HUD < Gtk::Window
	def initialize
				
		@amountToUpload=1 # quantité de logs voulus, indexée à 1	
				
		super("Logs uploader")
		
		css=Gtk::CssProvider.new
		css.load(path: "./style.css")
		Gtk::StyleContext::add_provider_for_screen(Gdk::Screen.default,css,
												   Gtk::StyleProvider::PRIORITY_APPLICATION)


		@logsListModel=Gtk::ListStore.new(String)
		
		# Génération des bosses

		path=File.expand_path("~/Documents/Guild\ Wars\ 2/addons/arcdps/arcdps.cbtlogs/")
		dirs=Pathname(path).children.select(&:directory?)

		wings=[]
		dirs.each { |path|
			BossesUtility.generateBoss(path.to_s.encode("utf-8"), wings, @logsListModel) 
		}
		wings.compact!
		wings.each { |w| w.initLogsPosition(@logsListModel) }

		
		boxMainUI=Gtk::Box.new(Gtk::Orientation::VERTICAL)
		boxUploadMenus=Gtk::Box.new(Gtk::Orientation::HORIZONTAL)
		boxWingsList=Gtk::Box.new(Gtk::Orientation::VERTICAL)
		boxAmountLogsChoice=Gtk::Box.new(Gtk::Orientation::HORIZONTAL)
		
		labelAmountLogsChoice=Gtk::Label.new("Quantité de logs à upload par rencontre : ")
		buttonAmountLogsChoice=Gtk::SpinButton.new(1, 20, 1)
		buttonAmountLogsChoice.numeric=true
		buttonAmountLogsChoice.digits=0
		buttonAmountLogsChoice.signal_connect('value_changed') { |btn|
			@amountToUpload=btn.value_as_int
		}
		
		btnUpload=initBtnUpload(wings)
		renderer=Gtk::CellRendererText.new
		renderer.size_points=8
		renderer.ypad=3
		# renderer.set_fixed_height_from_font(1)
		logsListView=Gtk::TreeView.new(@logsListModel)
		logsListView.selection.mode=:multiple
		logsListView.headers_visible=false
		col=Gtk::TreeViewColumn.new("Test", renderer, text: 0)
		logsListView.hexpand=Gtk::Align::FILL

		# exemples de test
		# iter=@logsListModel.append
		# iter[0]="https://dps.report/QzX5-20190716-185442_golem Golem chat standard"
		# iter=@logsListModel.append
		# iter[0]="https://dps.report/jv4Q-20190716-225341_golem Golem chat intermédiaire"
		
		logsListView.append_column(col)
		
		logsListView.signal_connect('row-activated') {	 |tree, path|
			
			str=""
		
			logsListView.selection.each { |model, path, iter|
				val=model.get_iter(path).get_value(0)
				str+=val if val!=nil
			}
			Clipboard.copy(str) if str!=""
		}


		
		wings.each do |wing|
			# next if wing==nil
			boxWingsList.add(wing)
			wing.showWing
		end 


		boxAmountLogsChoice.add(labelAmountLogsChoice)
		boxAmountLogsChoice.add(buttonAmountLogsChoice)
		
		boxUploadMenus.add(boxWingsList)
		boxUploadMenus.add(logsListView)
		
		boxMainUI.add(Gtk::Label.new(""+path))
		boxMainUI.add(boxAmountLogsChoice)
		boxMainUI.add(boxUploadMenus)
		boxMainUI.add(btnUpload)
		
		self.add(boxMainUI)
		self.signal_connect("destroy") { 
			Gtk.main_quit 
		}

		self.show_all
		Gtk.main
	end

	private
	def initBtnUpload(data)
		btnUpload=Gtk::Button.new(label: "Upload")
		btnUpload.signal_connect('clicked') do		
			data.each do |wing|
				wing.upload(@logsListModel, @amountToUpload)
			end
		end	

		btnUpload
	end
	
end